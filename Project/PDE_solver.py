import numpy as np
import tensorflow as tf
from keras.layers import Dense
from matplotlib import pyplot as plt


def der(f, x):
    res = [(f[1] - f[0]) / (x[1] - x[0])] * 2
    res.extend([(f[i] - f[i - 1]) / (x[i] - x[i - 1]) for i in range(2, len(f))])
    return res


def der2(f, x):
    res = [(f[0] - 2 * f[1] + f[2]) / ((x[1] - x[0]) * (x[2] - x[1]))] * 2
    res.extend(
        [(f[i - 1] - 2 * f[i] + f[i + 1]) / ((x[i] - x[i - 1]) * (x[i + 1] - x[i])) for i in range(2, len(f) - 2)])
    res.extend([(f[-3] - 2 * f[-2] + f[-1]) / ((x[-1] - x[-2]) * (x[-2] - x[-3]))] * 2)
    return res


def der2p(f, x, point):
    if point < 1:
        point = 1
    elif point > len(f) - 2:
        point = len(f) - 2
    return (f[point - 1] - 2 * f[point] + f[point + 1]) / ((x[point] - x[point - 1]) ** 2)


def loss(model, x, dim1, dim2):
    y_ = model(x)
    s = 0
    for inputs, result in zip(x, y_):
        res_matrix = tf.reshape(result, (dim1, dim2))
        transposed = tf.transpose(res_matrix)
        for i in range(1, len(res_matrix) - 1):
            for j in range(1, len(transposed) - 1):
                si = 0
                si += der2p(res_matrix[i], inputs[:dim1], j)
                si += der2p(transposed[j], inputs[dim1:], i)
                si += 1500 * np.exp(-0.007 * ((inputs[i] - 5) ** 2) * ((inputs[dim1 + j] - 5) ** 2)) / 4

                s += abs(si)

        for i in range(len(res_matrix)):
            s += abs(res_matrix[i][0] - 300) + abs(res_matrix[i][-1] - 300)
        for i in range(len(transposed)):
            s += abs(res_matrix[0][i] - 300) + abs(res_matrix[-1][i] - 300)

    return s / (dim1 * dim2)


def grad(model, inputs, dim1, dim2):
    with tf.GradientTape() as tape:
        loss_value = loss(model, inputs, dim1, dim2)
    return loss_value, tape.gradient(loss_value, model.trainable_variables)


def solve(xmin, xmax, zmin, zmax, dim1, dim2):
    x = np.linspace(xmin, xmax, num=dim1)
    x = np.append(x, np.linspace(zmin, zmax, num=dim2))
    y = 0
    train_dataset = tf.data.Dataset.from_tensor_slices(([[x]], [[y]]))

    model = tf.keras.Sequential([
        Dense(20, input_shape=(dim1 + dim2,), activation=tf.nn.relu),  # input shape required
        Dense(dim1 * dim2, dtype='float64')
    ])

    optimizer = tf.keras.optimizers.Adam(learning_rate=0.1)

    epoch = 0
    err = 20
    avg_loss = err
    update_epoch = 5
    while avg_loss >= err:
        epoch += 1
        epoch_loss_avg = tf.keras.metrics.Mean()
        for x, y in train_dataset:
            loss_value, grads = grad(model, x, dim1, dim2)
            optimizer.apply_gradients(zip(grads, model.trainable_variables))
            epoch_loss_avg.update_state(loss_value ** 2)

        if epoch % update_epoch == 0:
            avg_loss = epoch_loss_avg.result() ** 0.5
            print("Epoch {:03d}: Loss: {:.3f}".format(epoch, avg_loss))

    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111, projection='3d')
    for x, y in train_dataset:
        y_ = model(x)
        X, Z = np.meshgrid(x.numpy()[0][:dim1], x.numpy()[0][dim1:])
        Y = y_.numpy()[0].reshape(X.shape)
        ax.plot_surface(X, Z, Y)
        plt.show()
        return y_.numpy()[0].reshape((dim1, dim2))


if __name__ == "__main__":
    res = solve(0, 10, 0, 10, 9, 9)
    np.savetxt("results.txt", res)
