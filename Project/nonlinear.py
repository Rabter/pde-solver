import numpy as np
import tensorflow as tf
from keras.layers import Dense
from matplotlib import pyplot as plt

def der(f, x):
    res = [(f[1] - f[0]) / (x[1] - x[0])] * 2
    res.extend([(f[i] - f[i - 1]) / (x[i] - x[i - 1]) for i in range(2, len(f))])
    return res

def derp(f, x, point):
    if point < 0:
        point = 0
    elif point > len(f) - 2:
        point = len(f) - 2
    return (f[point + 1] - f[point]) / (x[point + 1] - x[point])

def der2p(f, x, point):
    if point < 1:
        point = 1
    elif point > len(f) - 2:
        point = len(f) - 2
    return (f[point - 1] - 2 * f[point] + f[point + 1]) / ((x[point] - x[point - 1]) ** 2)


k_keys = [temp for temp in range(300, 1301, 100)]
k_table = {300: 4.01, 400: 3.93, 500: 3.86, 600: 3.79, 700: 3.73, 800: 3.66, 900: 3.59, 1000: 3.52, 1100: 3.46,
           1200: 3.39, 1300: 3.32}


def k(u):
    low = 0
    length = len(k_keys)
    high = length - 1
    mid = high // 2
    while 0 < mid < length - 1 and not k_keys[mid] <= u <= k_keys[mid + 1]:
        if u < k_keys[mid]:
            high = mid - 1
        else:
            low = mid + 1
        mid = low + (high - low) // 2

    if mid == length - 1:
        mid -= 1

    left = k_keys[mid]
    right = k_keys[mid + 1]

    return k_table[left] + (k_table[right] - k_table[left]) * (u - left) / (right - left)


def f(inputs, i, j, sep):
    return 1500 * np.exp(-0.007 * ((inputs[i] - 5) ** 2) * ((inputs[sep + j] - 5) ** 2))

def loss(model, x, dim1, dim2):
    y_ = model(x)

    s = 0
    for inputs, result in zip(x, y_):
        res_matrix = tf.reshape(result, (dim1, dim2))
        transposed = tf.transpose(res_matrix)
        ku = np.zeros((dim1, dim2))
        for i in range(len(res_matrix)):
            for j in range(len(res_matrix[i])):
                ku[i][j] = k(res_matrix[i][j])
        ktran = np.transpose(ku)
        for i in range(1, len(res_matrix) - 1):
            dx = der(res_matrix[i], inputs[:dim1])
            dz = der(transposed[i], inputs[dim1:])
            for j in range(1, len(transposed) - 1):
                s += abs(derp(ku[i] * dx, inputs[:dim1], j) + derp(ktran[j] * dz, inputs[dim1:], i) + f(inputs, i, j, dim1))
               # ku = k(res_matrix[i][j])
               # s += abs(der2p(res_matrix[i], inputs[:dim1], j) + der2p(
               #     transposed[j], inputs[dim1:], i) + f(inputs, res_matrix[i][j], i, j, dim1))

        for i in range(len(res_matrix)):
            s += abs(res_matrix[0][i] - k(res_matrix[0][i]) * derp(res_matrix[0], inputs[:dim1], i))
            s += abs(res_matrix[-1][i] - k(res_matrix[-1][i]) * derp(res_matrix[-1], inputs[:dim1], i))
            s += abs(transposed[0][i] - k(transposed[0][i]) * derp(transposed[0], inputs[dim1:], i))
            s += abs(transposed[-1][i] - k(transposed[-1][i]) * derp(transposed[-1], inputs[dim1:], i))
        # for i in range(len(res_matrix)):
        #     s += abs(res_matrix[i][0] - 300) + abs(res_matrix[i][-1] - 300)
        # for i in range(len(transposed)):
        #     s += abs(res_matrix[0][i] - 300) + abs(res_matrix[-1][i] - 300)

    return s / (dim1 * dim2)


def loss_collocation(model, x, l, dim1, dim2, points):
    y_ = model(x)

    s = 0
    for inputs, result in zip(x, y_):
        si = 0
        for i in range(dim1):
            for j in range(dim2):
                sk = 0
                u = 0
                for coef in range(points):
                    num = np.pi * (2 * coef + 1) / l
                    sk += result[coef] * (num ** 2) * np.sin(num * inputs[i]) * np.sin(num * inputs[dim1 + j])
                    u += result[coef] * np.sin(num * inputs[i]) * np.sin(num * inputs[dim1 + j])
                si += abs(f(inputs, i, j, dim1) - 2 * k(u) * sk)
                #si += abs(f(inputs, u, i, j, dim1) - 2 * sk)
        s += si / (dim1 * dim2)

    return s


def grad(model, inputs, dim1, dim2):
    with tf.GradientTape() as tape:
        loss_value = loss(model, inputs, dim1, dim2)
    return loss_value, tape.gradient(loss_value, model.trainable_variables)


def grad_collocation(model, inputs, l, dim1, dim2, points):
    with tf.GradientTape() as tape:
        loss_value = loss_collocation(model, inputs, l, dim1, dim2, points)
    return loss_value, tape.gradient(loss_value, model.trainable_variables)


def solve(xmin, xmax, zmin, zmax, dim1, dim2):
    x = np.linspace(xmin, xmax, num=dim1)
    x = np.append(x, np.linspace(zmin, zmax, num=dim2))
    y = 0

    model = tf.keras.Sequential([
        Dense(20, input_shape=(dim1 + dim2,), activation=tf.nn.relu),  # input shape required
        Dense(dim1 * dim2, dtype='float64')
    ])

    optimizer = tf.keras.optimizers.Adam(learning_rate=100)

    epoch = 0
    err = 160
    avg_loss = err
    train_dataset = tf.data.Dataset.from_tensor_slices(([[x]], [[y]]))
    while avg_loss >= err:
        epoch += 1
        epoch_loss_avg = tf.keras.metrics.Mean()
        for x, y in train_dataset:
            loss_value, grads = grad(model, x, dim1, dim2)
            optimizer.apply_gradients(zip(grads, model.trainable_variables))
            epoch_loss_avg.update_state(loss_value ** 2)

        avg_loss = epoch_loss_avg.result() ** 0.5
        print("Epoch {:03d}: Loss: {:.3f}".format(epoch, avg_loss))

    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111, projection='3d')
    Y = None
    for x, y in train_dataset:
        y_ = model(x)
        X, Z = np.meshgrid(x.numpy()[0][:dim1], x.numpy()[0][dim1:])
        Y = y_.numpy()[0].reshape(X.shape)
        ax.plot_surface(X, Z, Y)

    plt.show()
    return Y


def solve_collocation(xmin, xmax, zmin, zmax, dim1, dim2, showdim, points):
    x = np.linspace(xmin, xmax, num=dim1 + 2)[1:-1]
    x = np.append(x, np.linspace(zmin, zmax, num=dim2 + 2)[1:-1])
    y = 0
    train_dataset = tf.data.Dataset.from_tensor_slices(([[x]], [[y]]))

    model = tf.keras.Sequential([
        Dense(dim1 + dim2, input_shape=(dim2 + dim2,), activation=tf.nn.relu),  # input shape required
        Dense(points, dtype='float64')
    ])

    base_lr = 100
    optimizer = tf.keras.optimizers.Adam(learning_rate=base_lr)

    epoch = 0
    err = 50#78
    avg_loss = err
    while avg_loss >= err:
        epoch += 1
        epoch_loss_avg = tf.keras.metrics.Mean()
        for x, y in train_dataset:
            loss_value, grads = grad_collocation(model, x, xmax - xmin, dim1, dim2, points)
            optimizer.apply_gradients(zip(grads, model.trainable_variables))
            epoch_loss_avg.update_state(loss_value ** 2)

        avg_loss = epoch_loss_avg.result() ** 0.5
        print("Epoch {:03d}: Loss: {:.3f}".format(epoch, avg_loss))
    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111, projection='3d')

    x_ = [x for x, y in train_dataset][0]

    x = np.linspace(xmin, xmax, num=showdim)
    z = np.linspace(zmin, zmax, num=showdim)
    X, Z = np.meshgrid(x, z)
    res = model(x_).numpy()[0]
    Y = np.array([[300 for _ in range(showdim)] for _ in range(showdim)])
    l = x[-1] - x[0]
    for i in range(showdim):
        for j in range(showdim):
            for k in range(len(res)):
                num = np.pi * (2 * k - 1) / l
                Y[i][j] += res[k] * np.sin(num * x[i]) * np.sin(num * z[j])

    ax.plot_surface(X, Z, Y)
    plt.show()

    return Y


if __name__ == "__main__":
    #res = solve(0, 10, 0, 10, 10, 10)
    res = solve_collocation(0, 10, 0, 10, 10, 10, 100, 10)
    np.save("results.txt", res)
