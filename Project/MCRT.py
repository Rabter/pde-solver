import random
import time
import numpy as np
from matplotlib import pyplot as plt

EPS = 1e-3


class RunThroughParams:
    def __init__(self, K0, KN, M0, MN, P0, PN, size):
        self.K0 = K0
        self.KN = KN
        self.M0 = M0
        self.MN = MN
        self.P0 = P0
        self.PN = PN
        self.size = size
        self.A = np.zeros(size)
        self.B = np.zeros(size)
        self.C = np.zeros(size)
        self.D = np.zeros(size)


def run_through(params):
    size = params.size
    ksi = np.zeros(size)
    etta = np.zeros(size)
    ksi[0] = -params.M0 / params.K0
    etta[0] = params.P0 / params.K0

    for i in range(1, size):
        ksi[i] = -params.C[i] / (params.B[i] + params.A[i] * ksi[i - 1])
        etta[i] = (params.D[i] - params.A[i] * etta[i - 1]) / (params.B[i] + params.A[i] * ksi[i - 1])

    res = np.zeros(size)
    res[size - 1] = (params.PN - params.MN * etta[size - 1]) / (params.KN + params.MN * ksi[size - 1])

    for i in range(size - 2, -1, -1):
        res[i] = ksi[i] * res[i + 1] + etta[i]

    return res


class Plate:
    def __init__(self):
        self.dx = self.dz = 0
        self.width = self.height = 10
        self.x_points_amount = 101
        self.z_points_amount = 101
        self.Tenv = 300
        self.k = 4
        self.tau = 0.1

        self.iterations_amount = 2000

    def f(self, x, z, a, b):
        f = 1500
        beta = -0.007
        return f * np.exp(beta * pow(x - a / 2, 2) * pow(z - b / 2, 2)) #* abs((a / 2) - abs((a / 2) - x)) * abs((b / 2) - abs((b / 2) - z)) * (4 / a / b)

    def calculate_approximation(self):
        dx = self.width / (self.x_points_amount - 1)
        dz = self.height / (self.z_points_amount - 1)
        x = np.linspace(0, self.width, self.x_points_amount)
        z = np.linspace(0, self.height, self.z_points_amount)

        params = RunThroughParams(1, 1, 0, 0, self.Tenv, self.Tenv, self.z_points_amount)
        Tcur = np.ndarray((self.x_points_amount, self.z_points_amount))
        Tcur.fill(self.Tenv)

        go = True
        while go:
            Tprev = Tcur.copy()

            A = 1 / (dx * dx)
            B = -2 / (dx * dx) - 2 / self.tau
            for i in range(self.x_points_amount):
                for j in range(1, self.z_points_amount - 1):
                    params.A[j] = A
                    params.B[j] = B
                    params.C[j] = A
                    params.D[j] = - Tcur[i][j - 1] / (dz * dz) + Tcur[i][j] * (2 / (dz * dz) - 2 / self.tau) - Tcur[i][
                        j + 1] / (dz * dz) - self.f(x[i], z[j], self.width, self.height) / self.k

                params.A[0] = params.A[1]
                params.B[0] = params.B[1]
                params.C[0] = params.C[1]
                params.D[0] = params.D[1]
                params.A[self.z_points_amount - 1] = params.A[self.z_points_amount - 2]
                params.B[self.z_points_amount - 1] = params.B[self.z_points_amount - 2]
                params.C[self.z_points_amount - 1] = params.C[self.z_points_amount - 2]
                params.D[self.z_points_amount - 1] = params.D[self.z_points_amount - 2]

                Tcur[i] = run_through(params)

            A = 1 / (dz * dz)
            B = -2 / (dz * dz) - 2 / self.tau
            for j in range(self.z_points_amount):
                for i in range(1, self.x_points_amount - 1):
                    params.A[i] = A
                    params.B[i] = B
                    params.C[i] = A
                    params.D[i] = - Tcur[i - 1][j] / (dx * dx) + Tcur[i][j] * (2 / (dx * dx) - 2 / self.tau) - \
                                  Tcur[i + 1][j] / (dx * dx) - self.f(x[i], z[j], self.width, self.height) / self.k

                params.A[0] = params.A[1]
                params.B[0] = params.B[1]
                params.C[0] = params.C[1]
                params.D[0] = params.D[1]
                params.A[self.x_points_amount - 1] = params.A[self.x_points_amount - 2]
                params.B[self.x_points_amount - 1] = params.B[self.x_points_amount - 2]
                params.C[self.x_points_amount - 1] = params.C[self.x_points_amount - 2]
                params.D[self.x_points_amount - 1] = params.D[self.x_points_amount - 2]

                Tx = run_through(params)
                for i in range(len(Tx)):
                    Tcur[i][j] = Tx[i]
            go = abs(Tcur - Tprev).max() > EPS

        return x, z, Tcur

    def calculate_probability(self):
        self.dx = self.width / (self.x_points_amount - 1)
        self.dz = self.height / (self.z_points_amount - 1)
        x = np.linspace(0, self.width, self.x_points_amount)
        z = np.linspace(0, self.height, self.z_points_amount)

        Tcur = np.ndarray((self.x_points_amount, self.z_points_amount))
        Tcur.fill(self.Tenv)

        for layer in range(1, min(self.x_points_amount, self.z_points_amount) // 2):
            for i in range(layer, self.x_points_amount - layer):
                self.walk(Tcur, x, z, i, layer, layer)
                self.walk(Tcur, x, z, i, self.z_points_amount - 1 - layer, layer)
            for i in range(layer + 1, self.z_points_amount - 1 - layer):
                self.walk(Tcur, x, z, layer, i, layer)
                self.walk(Tcur, x, z, self.x_points_amount - 1 - layer, i, layer)

        return x, z, Tcur

    def walk(self, Tcur, x, z, i0, j0, layer):
        tmp = 0
        iterations_amount = self.iterations_amount
        for it in range(iterations_amount):
            i = i0
            j = j0
            fsum = self.f(x[i], z[j], self.width, self.height)
            jumps = 0
            go = True
            while go:
                chance = random.randint(0, 3) % 4
                if chance == 1:
                    i += 1
                elif chance == 2:
                    j += 1
                elif chance == 3:
                    i -= 1
                else:
                    j -= 1

                fsum += self.f(x[i], z[j], self.width, self.height)

                jumps += 1
                go = layer <= i < (self.x_points_amount - layer) and layer <= j < (self.z_points_amount - layer)
            tmp += fsum * self.dx * self.dz / (4 * self.k) + Tcur[i][j]

        Tcur[i0][j0] = tmp / iterations_amount


plate = Plate()
start = time.time()
x, z, y = plate.calculate_approximation()
# x, z, y = plate.calculate_probability()
end = time.time()

fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111, projection='3d')
x, z = np.meshgrid(x, z)
ax.plot_surface(x, z, y, color='red')
plt.show()
print()

# N = 101
# acc = np.loadtxt("accurate.txt")
# mul = 100 / (N - 1)
# s = 0
# sr = 0
# res = y
# for i in range(len(res)):
#     for j in range(len(res[i])):
#         s += abs(acc[int(i * mul)][int(j * mul)] - res[i][j])
#         sr += abs(acc[int(i * mul)][int(j * mul)] - res[i][j]) / acc[int(i * mul)][int(j * mul)]
# s /= len(res) ** 2
# sr /= len(res) ** 2
# print(s, sr, (end - start) * 1000)
