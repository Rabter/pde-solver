import time
import numpy as np
import tensorflow as tf
from keras.layers import Dense
from matplotlib import pyplot as plt


def f(x, z):
    return 1500 * np.exp(-0.007 * ((x - 5) ** 2) * ((z - 5) ** 2)) / 4


def loss(model, x, l, dim1, dim2, points):
    y_ = model(x)

    s = 0
    for inputs, result in zip(x, y_):
        si = 0
        for i in range(dim1):
            for j in range(dim2):
                sk = 0
                for k in range(points):
                    num = np.pi * (2 * k + 1) / l
                    sk += result[k] * (num ** 2) * np.sin(num * inputs[i]) * np.sin(num * inputs[dim1 + j])
                si += abs(f(inputs[i], inputs[dim1 + j]) - 2 * sk)
        s += si / (dim1 * dim2)

    return s


def grad(model, inputs, l, dim1, dim2, points):
    with tf.GradientTape() as tape:
        loss_value = loss(model, inputs, l, dim1, dim2, points)
    return loss_value, tape.gradient(loss_value, model.trainable_variables)


def solve(xmin, xmax, zmin, zmax, dim1, dim2, showdim, points):
    x = np.linspace(xmin, xmax, num=dim1 + 2)[1:-1]
    x = np.append(x, np.linspace(zmin, zmax, num=dim2 + 2)[1:-1])
    y = 0
    train_dataset = tf.data.Dataset.from_tensor_slices(([[x]], [[y]]))

    model = tf.keras.Sequential([
        Dense(dim1 + dim2, input_shape=(dim2 + dim2,), activation=tf.nn.relu),  # input shape required
        # Dense(10000, activation=tf.nn.relu),
        Dense(points, dtype='float64')
    ])

    base_lr = 50
    optimizer = tf.keras.optimizers.Adam(learning_rate=base_lr)

    epoch = 0
    err = 78
    avg_loss = err
    while avg_loss >= err:
        epoch += 1
        epoch_loss_avg = tf.keras.metrics.Mean()
        for x, y in train_dataset:
            loss_value, grads = grad(model, x, xmax - xmin, dim1, dim2, points)
            optimizer.apply_gradients(zip(grads, model.trainable_variables))
            epoch_loss_avg.update_state(loss_value ** 2)

        avg_loss = epoch_loss_avg.result() ** 0.5
        print("Epoch {:03d}: Loss: {:.6f}".format(epoch, avg_loss))

    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111, projection='3d')

    x_ = [x for x, y in train_dataset][0]

    x = np.linspace(xmin, xmax, num=showdim)
    z = np.linspace(zmin, zmax, num=showdim)
    X, Z = np.meshgrid(x, z)
    res = model(x_).numpy()[0]
    Y = np.array([[300 for _ in range(showdim)] for _ in range(showdim)])
    l = x[-1] - x[0]
    for i in range(showdim):
        for j in range(showdim):
            for k in range(len(res)):
                num = np.pi * (2 * k - 1) / l
                Y[i][j] += res[k] * np.sin(num * x[i]) * np.sin(num * z[j])

    ax.plot_surface(X, Z, Y)
    plt.show()

    return Y


if __name__ == "__main__":
    res = solve(0, 10, 0, 10, 10, 10, 100, 2)
    np.savetxt("collocation.txt", res)
