import numpy as np
import tensorflow as tf
from keras.layers import Dense
from matplotlib import pyplot as plt


# ToDo: interpolate
def der(f, x):
    res = [(f[1] - f[0]) / (x[1] - x[0])] * 2
    res.extend([(f[i] - f[i - 1]) / (x[i] - x[i - 1]) for i in range(2, len(f))])
    return res


def solution(x):
    return np.exp(-(x ** 2) / 2) / (x ** 3 + x + 1) + x ** 2


def accuracy(model, x):
    y = model(x)
    res = list()
    for xi, yi in zip(x, y):
        correct = solution(xi)
        lq = (1 + 3 * (xi ** 2)) / (1 + xi + xi ** 3)
        acc = abs((der(yi, xi) + (xi + lq) * yi - xi ** 3 - 2 * xi - lq * xi * xi))
        res.append((acc, abs((correct - yi) / correct), abs(acc / correct)))
    return res


def loss(model, x, y, tape, training):
    y_ = model(x)

    s = 0
    for xi, yi in zip(x, y_):
        lq = (1 + 3 * (xi ** 2)) / (1 + xi + xi ** 3)
        s += tf.reduce_sum(abs((der(yi, xi) + (xi + lq) * yi - xi ** 3 - 2 * xi - lq * xi * xi)))
        s += abs(yi[0] - 1)

    return s / tf.size(x).numpy()


def grad(model, inputs, targets):
    with tf.GradientTape() as tape:
        loss_value = loss(model, inputs, targets, tape, training=True)
    return loss_value, tape.gradient(loss_value, model.trainable_variables)


# Keep results for plotting
train_loss_results = []
train_accuracy_results = []

num_epochs = 1501

x = np.linspace(0, 50)
y = 0
train_dataset = tf.data.Dataset.from_tensor_slices(([[x]], [[y]]))

model = tf.keras.Sequential([
    Dense(50, input_shape=(50,), activation=tf.nn.relu),  # input shape required
    # Dense(100, activation=tf.nn.relu),
    Dense(50, dtype='float64')
])

optimizer = tf.keras.optimizers.SGD(learning_rate=0.00001)

for epoch in range(num_epochs):
    epoch_loss_avg = tf.keras.metrics.Mean()
    # epoch_accuracy = tf.keras.metrics.SparseCategoricalAccuracy()

    # Training loop - using batches of 32
    for x, y in train_dataset:
        # Optimize the model
        loss_value, grads = grad(model, x, y)
        optimizer.apply_gradients(zip(grads, model.trainable_variables))

        # Track progress
        epoch_loss_avg.update_state(loss_value)  # Add current batch loss
        # Compare predicted label to actual label
        # training=True is needed only if there are layers with different
        # behavior during training versus inference (e.g. Dropout).
        # epoch_accuracy.update_state(y, model(x, training=True))

    # End epoch
    train_loss_results.append(epoch_loss_avg.result())
    # train_accuracy_results.append(epoch_accuracy.result())

    if epoch % 50 == 0:
        avg_loss = epoch_loss_avg.result()
        optimizer.learning_rate = avg_loss / 1000
        print("Epoch {:03d}: Loss: {:.3f}".format(epoch, avg_loss))
    #     print("Epoch {:03d}: Loss: {:.3f}, Accuracy: {:.3%}".format(epoch,
    #                                                                 epoch_loss_avg.result(),
    #                                                                 epoch_accuracy.result()))

x = np.linspace(0, 50)
sol = solution(x)
plt.plot(x, sol, 'black')
for x, y in train_dataset:
    y_ = model(x)
    plt.plot(x[0], y_[0])
    acc = accuracy(model, x)
    print(tf.reduce_mean(acc[0][1]))
    print(tf.reduce_mean(acc[0][2]))
    print()
plt.show()

